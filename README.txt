This module adds input fields for street number (house number) and extension to
an addressfield widget. It shows those input fields only for certain countries;
for other countries the 'regular' 2 thoroughfare + premise fields are shown.

House numbers / extensions have no designated storage in addressfields at this
moment, and this module stores the numbers and extensions in the
'thoroughfare' field in the database. It splits them back out from the
'thoroughfare' for editing in separate input fields.

REASONING

The Addressfield module does not have separate storage for street numbers; at
least, not officially.

If web sites (or modules like commerce_klarna v7.x-1.x) at this moment provide
an input element for street number/suffix, they probably use the premise and/or
sub_premise fields to store these. This does not adhere to the official xNAL
specifications, which can cause incompatibilities with a.o. payment modules that
expect the full address stored in the 'thoroughfare' field (e.g.
commerce_paypal). This is why the Addressfield module will not support storing
street numbers in premise/sub_premise fields.
(https://drupal.org/node/2115749#comment-8245025)

Before adding 'official' extra fields for thoroughfare number/suffix to the
Addressfield module, there will need to be a decision on how storage is handled,
so that modules like commerce_paypal will know what to expect.
(This could be a method like the name. first_name and last_name fields in the
database, which essentially hold duplicate data for recent versions of
Addressfield.)

Until that 'official' storage method is clear and Adressfield can really support
street numbers/suffixes, this module will do. It keeps commerce_paypal working
because all user input gets stored into existing fields.


HARDCODED ASSUMPTIONS / LIMITATIONS

This module has some hardcoded assumptions:

1. the list(s) of countries which need a separate house number and/or suffix;
   see & edit near the end of the .module file.

2. the way a number/suffix properties split off from a thoroughfare
   (in regular expressions)

3. the way the properties are concatenated when saving them into the
   'thoroughfare' field.

1 and 2 could be made configurable or extended to be always good. Point 3
can't really.
At the moment, the module uses spaces to concatenate all fields. This means
that e.g. an address like 'Main Street / 7 / b' will be saved as
"Main Street 7 b", and cannot be saved as e.g. "Main Street 7b" or
"Main Street 7-b".

This last thing may not make it suitable to be a fully supported contrib module.
