<?php

/**
 * @file
 * Hides the 'premise' field and shows thoroughfare number(+ extension) instead.
 *
 * This is an addressfield formatter plugin.
 *
 * Note nothing in this plugin/module dictates that 'premise' should be hidden;
 * we just assume it does. A 'do not hide premise-countries' array could easily
 * be implemented, below.
 */

$plugin = array(
  'title' => t('Replace premise by thoroughfare-number/extension'),
  'format callback' => 'addressfield_tfnr_format_address_tfnr',
  'type' => 'address',
  // This should be processed after the 'address' plugin:
  'weight' => -81,
);

/**
 * Format callback.
 *
 * @see CALLBACK_addressfield_format_callback()
 */
function addressfield_tfnr_format_address_tfnr(&$format, &$address, $context = array()) {

  if ($context['mode'] == 'form') {

    // Merge or split thoroughfare/number/ext sub-fields, depending on
    // the country settings. On 'normal' form build, we will only split but on
    // form-rebuild for an AJAX callback we may need to merge them back.
    // Since $address is passed by reference, thoroughfare gets modified for the
    // caller (addressfield_generate()) and the element #process functions.
    $number_ext = addressfield_tfnr_addressfield_has_number($address['country']);
    addressfield_tfnr_change_thoroughfare($address, $number_ext);

    if ($number_ext) {
      // Create extra form elements, with the split-out values.
      // Since these are not supported by addressfield (yet?), we
      // need to supply a full form element with #type and #default_value.
      $format['street_block']['thoroughfare_number'] = array(
        '#title' => '', // t('Number'),
        '#tag' => NULL,
        '#attributes' => array('class' => array('thoroughfare-number')),
        '#size' => 4,
        '#prefix' => ' ',
        '#type' => 'textfield',
        '#default_value' => !empty($address['thoroughfare_number']) ? $address['thoroughfare_number'] : '',
      );
      if ($number_ext > 1) {
        $format['street_block']['thoroughfare_ext'] = array(
          '#title' => '', // t('Ext.'),
          '#tag' => NULL,
          '#attributes' => array('class' => array('thoroughfare-ext')),
          '#size' => 2,
          '#prefix' => ' ',
          '#type' => 'textfield',
          '#default_value' => !empty($address['thoroughfare_ext']) ? $address['thoroughfare_ext'] : '',
        );
      }

      // Change CSS to display items next to each other.
      if (empty($format['street_block']['#attributes']['class'])) {
        $format['street_block']['#attributes']['class'] = array('addressfield-container-inline');
      }
      elseif (!in_array('addressfield-container-inline', $format['street_block']['#attributes']['class'])) {
        $format['street_block']['#attributes']['class'][] = 'addressfield-container-inline';
      }

      // Hide premise; don't delete it in case it still holds data, so it's
      // still there after you switch countries. We need to empty it on submit.
      // (Note that's different from how addressfield treats e.g. postal_code;
      // if you switch to a country without code and back, the value is gone.)
      $format['street_block']['premise']['#access'] = FALSE;
    }
  }
}
